﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTwoStick : MonoBehaviour {

    public Transform target;
    public Transform target2;
    public Vector3 zOffset = new Vector3(0, 0, -10);
    public Rect window = new Rect(0, 0, 1, 1);

    Vector3 targetPos;
    Vector2 targetViewPos;
    void Update()
    {
        // get the position of the target
        float bigger = System.Math.Max(target.position.magnitude, target2.position.magnitude);
        float smal = System.Math.Min(target.position.magnitude, target2.position.magnitude);
        
        targetPos = bigger - smal;
            target.position - target2.position; // world coords
        // convert to viewport coordinates
      targetViewPos =
            Camera.main.WorldToViewportPoint(targetPos);
        /*
        // clamp this position to 
        // the closest point inside the window
        Vector2 goalViewPos = window.Clamp(targetViewPos);

        // convert back to world coordinates
        Vector3 goalPos =
            Camera.main.ViewportToWorldPoint(goalViewPos);

        // convert both points into the local coordinate
        // system of the camera
        targetPos = transform.InverseTransformPoint(targetPos);
        goalPos = transform.InverseTransformPoint(goalPos);

        // compute the necessary camera movement in the xy plane
        // for the target to appear at the goal
        Vector3 move = targetPos - goalPos;
        move.z = 0;

        transform.Translate(move);*/
        

    }

    private void FixedUpdate()
    {
        if (targetViewPos.Equals(targetPos))
        {
            Camera.main.orthographicSize = Camera.main.orthographicSize;
        }
        else
        {
            Camera.main.orthographicSize = Camera.main.orthographicSize + 0.1f;
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        for (int i = 0; i < 4; i++)
        {
            Vector3 f =
             Camera.main.ViewportToWorldPoint(window.Corner(i))
              - zOffset;
            Vector3 t =
             Camera.main.ViewportToWorldPoint(window.Corner(i + 1))
              - zOffset;

            Gizmos.DrawLine(f, t);
        }

    }

}
